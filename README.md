FreshRSS Docker Container
=========================

_*FreshRSS is a self-hosted RSS feed aggregator such as [Leed](http://projet.idleman.fr/leed/) or [Kriss Feed](https://tontof.net/kriss/feed/).*_

_*It is at the same time lightweight, easy to work with, powerful and customizable.*_

_*It is a multi-user application with an anonymous reading mode.
It supports [PubSubHubbub](https://github.com/pubsubhubbub/PubSubHubbub) for instant notifications from compatible Web sites.
There is an API for (mobile) clients, and a [Command-Line Interface](freshrss/cli/README.md).
Finally, it supports [extensions](freshrss/#extensions) for further tuning.*_

This container aims to be easy to understand, easy to backup/restore, and easy to operate.

Any suggestions on improving this containers ease of use are welcome.

Running FreshRSS
-----------------

    docker run -d -p 80:80 --name freshrss \
		    -v /data/docker/freshrss:/var/www/FreshRSS/data \
        registry.gitlab.com/sparky8251/freshrss

If this is the first time creating a FreshRSS server using the above volumes,
you can access the installer by going to http://127.0.0.1/

If this is pointing to an existing install you can access FreshRSS by going to
http://127.0.0.1/

Upgrading FreshRSS
------------------------------

### Upgrading ###

Just stop the old container and run the new one with the same `-v` mappings. Since the data is on your docker host, you do not need to do anything special to preserve it between containers.
